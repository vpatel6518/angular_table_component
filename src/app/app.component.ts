import { Component, OnInit } from '@angular/core';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent  implements OnInit {
public tableData: Array<any>;
public rowKeys: Array<any>;

constructor(private appService: AppService) { }

ngOnInit() {
this.appService.getSampleData().subscribe((data: any) => {
this.tableData = data;
this.rowKeys = Object.keys(data[0]).map(keyName => ({label: keyName, value: keyName }));
});
}
}
