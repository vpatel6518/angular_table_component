import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { AppService } from './app.service';
import { of } from 'rxjs';
import { AppComponent } from './app.component';
import { TableComponent } from './table/table.component';
import { PaginationComponent } from './table/pagination/pagination.component';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';


const GRID_DATA = [{'id': 1}];

describe('AppComponent', () => {
  let component: AppComponent;
    let fixture: ComponentFixture<AppComponent>;
    let service: AppService
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      
      imports: [
        BrowserModule,
        HttpClientModule,
        FormsModule
      ],
      declarations: [
        AppComponent,
        TableComponent,
        PaginationComponent
      ],
      providers: [
        {provide: AppService, useValue: {
          getSampleData(){
            return of(GRID_DATA);
          }
        }}
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    service = TestBed.get(AppService);
    fixture.detectChanges();
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it('should call getSampleData from AppService', () => {
    spyOn(service, 'getSampleData').and.returnValue(of(GRID_DATA));
    component.ngOnInit();
    expect(service.getSampleData).toHaveBeenCalled();
  });

  it('should set tableData from response', () => {
    spyOn(service, 'getSampleData').and.returnValue(of(GRID_DATA));
    component.ngOnInit();
    expect(component.tableData).toBe(GRID_DATA);
  });
});
