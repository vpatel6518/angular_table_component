import { Component, OnInit, Input, ChangeDetectorRef, DoCheck } from '@angular/core';
import { AppService } from '../app.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit, DoCheck {
  @Input() data: Array<any> = [];
  @Input() columns: Array<any> = [];
  @Input() pagination = false;
  currentTableData: Array<any> = [];

  constructor(private cdr: ChangeDetectorRef, private appService: AppService) { }

    ngOnInit() {
      if (!this.pagination) {
        this.currentTableData = this.data;
      }
    }
    ngDoCheck() {
      this.cdr.detectChanges();
    }

  bindTableData(event) {
    const {currentPage, rowsPerPage} = event;
    const lastInd = currentPage * rowsPerPage;
    const startInd = (currentPage - 1) * rowsPerPage;
    this.currentTableData = this.data.slice(0).slice(startInd, lastInd);
  }

  submitRowData(item) {
    const payloadData = {rowStatus: item.status,
    rowId: item.id};
// post selected row data as payloadData
    this.appService.submitData(payloadData).subscribe((data: any) => {

    });
  }

}
