import { Component, OnInit, Input,  EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {
  @Input() numberOfItems: number;
  @Output() emitToBind: EventEmitter<any> = new EventEmitter();
  rowOptions = [10, 20, 30, 40, 50];
  rowsPerPage = 10;
  pageList: Array<number> = [];
  currentPageList: Array<number> = [];
  currentPage = 1;

  constructor() { }

    ngOnInit() {
      this.calculatePageList();
    }
 // set pagination list
  calculatePageList() {
      this.pageList = [];
      const pagesLength = this.numberOfItems / this.rowsPerPage;
      for (let i = 1; i < pagesLength + 1; i++) {
        this.pageList.push(i);
      }
      this.currentPageList = this.getCurrentPageList();
      this.emitItemsToBind();
  }

// set currentpage and how many rows should display for every paginate change
  emitItemsToBind() {
    console.log('data to bind  rowsPerPage===', this.rowsPerPage, 'currentPage----', this.currentPage);
    this.emitToBind.emit({
      rowsPerPage:  this.rowsPerPage,
      currentPage: this.currentPage
    });
  }
// get how many rows we have selected
  getCurrentPageList() {
   let startIndex = 0;
   console.log('currentPage===', this.currentPage);
   if (this.currentPage - 2 > 0) {
        startIndex = this.currentPage - 3;
        if (!(this.pageList.length - startIndex > 4)) {
        startIndex = this.pageList.length - 5;
        }
        if ((this.pageList.length === 4)) {
          startIndex = 0;
        }
      }
   return this.pageList.slice(0).splice(startIndex, 5);
  }

  rowsPerPageChange() {
    this.currentPage = 1;
    this.calculatePageList();
  }

  pageItemClick(pageNumber) {
    this.currentPage = pageNumber;
  //  this.calculatePageList();
    this.currentPageList = this.getCurrentPageList();
    this.emitItemsToBind();
  }

  nextPageItemClick() {
      if (this.currentPage < this.pageList.length) {
      // this.currentPage = this.currentPage == 1 ? this.currentPage + 3 : this.currentPage + 1;
      this.currentPage++;
      this.currentPageList = this.getCurrentPageList();
      this.emitItemsToBind();
      }
  }

  prevPageItemClick() {
  if (this.currentPage > 1) {
  // this.currentPage = this.currentPage == this.pageList.length ? this.currentPage - 3 : this.currentPage - 1;
      this.currentPage--;
      this.currentPageList = this.getCurrentPageList();
      this.emitItemsToBind();
  }
  }
// directly go to start page when you click dobule-left icon.
  goToStartPage() {
  this.currentPage = 1;
  this.currentPageList = this.getCurrentPageList();
  this.emitItemsToBind();
  }
// directly go to last page when you click dobule-right icon.
  goToLastPage() {
  this.currentPage = this.pageList.length;
  this.currentPageList = this.getCurrentPageList();
  this.emitItemsToBind();
  }

}
