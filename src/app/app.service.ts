import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private http: HttpClient) { }

  getSampleData() {
  return this.http.get('../assets/data/sample-data.json');
  }

  submitData(payload) {
  return this.http.post('/api/submit', payload);
  }
}
